# FontCSFS

### Environment requirement
> Operating System: `Ubuntu v18.04`
>
> NodeJS: `v12.16.1`
>
> NPM: `v6.13.4`

### Install Dependencies
> Install project dependencies: `npm install`

### Development (For Developer)
> Run: `npm start`
>
> Open browser at: `http://localhost:8080`
>
> Please install flug-in ESLINT into your IDE for code convention auto checking

### Develop Build
> Run: `npm run build:development`
>
> serve -s build
>
> Copy all content in `./build` for deployment

### Staging Build
> Run: `npm run build:staging`
>
> serve -s build
>
> Copy all content in `./build` for deployment

### Production Build
> Run: `npm run build`
>
> serve -s build
>
> Copy all content in `./build` for deployment

### Check Convention
> Javascript: `npm run lint`
>
> CSS: `npm run stylelint`
