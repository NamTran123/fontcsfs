import React, { Component, Suspense, lazy } from 'react'
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom'
import { ToastContainer } from 'react-toastify'

import Loading from '../components/loading'
import Page from '../components/page'

import './style.scss'

const Home = lazy(() => import('../pages/home'))
const NotFound = lazy(() => import('../pages/not-found'))

class Routes extends Component {
  renderLazyComponent = (LazyComponent, params) => (props) => <LazyComponent {...props} {...params} />

  render() {
    return (
      <Suspense fallback={<Page><Loading /></Page>}>
        <ToastContainer
          position="top-right"
          autoClose={5000}
          hideProgressBar={false}
          newestOnTop={false}
          closeOnClick
          rtl={false}
          pauseOnFocusLoss
          draggable
          pauseOnHover
          theme="colored"
        />
        <Router>
          <Switch>
            <Route path="/" exact component={this.renderLazyComponent(Home)} />
            <Route path="/not-found" component={this.renderLazyComponent(NotFound)} />
            <Redirect to="/not-found" />
          </Switch>
        </Router>
      </Suspense>
    )
  }
}

export default Routes
