import React, { useState } from "react";
import { Modal, Steps, Upload } from "antd";
import { InboxOutlined } from "@ant-design/icons";
import DropArea from "./drop-area";
import { ExternalApi } from "../../api/endpoint";
import { getBase64 } from "../../utils/file-reader";
import Result from "./result";

import "./style.scss";

const { Step } = Steps;

const { Dragger } = Upload;

let radioScale = 1;

function Home() {
  const [currentStep, setCurrentStep] = useState(0);
  const [previewImg, setPreviewImg] = useState("");
  const [inforPredict, setInforPredict] = useState([]);
  const [list, setList] = useState([]);
  const [showTest, setShowTest] = useState(false);
  const [fileOrigin, setFileOrigin] = useState(null);
  const [fileResult, setFileResult] = useState(null);
  const [popupInfo, setPopupInfo] = useState([]);
  const [dataPreview, setDataPreview] = useState({});

  const props = {
    name: "file",
    showUploadList: false,
    accept: "image/*",
    beforeUpload: () => false,
    onChange(info) {
      setInforPredict([]);

      const { file } = info;

      let formData = new FormData();

      formData.append("file", file);
      ExternalApi.post(
        "http://namecard-ai.softone.asia/api/name-card-detection/text-detection",
        formData
      )
        .then((res) => {
          console.log(res, "response123123123");
          if (res.data.ratio_scale) {
            radioScale = res.data.ratio_scale;
          }
          return res;
        })
        .then(async (res) => {
          setCurrentStep(1);
          if (res.status) {
            setList([]);
            setFileOrigin(file);
            await setPreviewImg(await getBase64(file));
            await setInforPredict(res.data?.coordinates || []);
          }
        });
    },
  };

  const dataURLtoFile = (dataUrl, filename) => {
    const formatData = `data:text/csv;base64,${dataUrl}`;
    let arr = formatData.split(",");
    let mime = arr[0].match(/:(.*?);/)[1];
    let bstr = atob(arr[1]);
    let n = bstr.length;
    let u8arr = new Uint8Array(n);

    while (n--) {
      u8arr[n] = bstr.charCodeAt(n);
    }

    return new File([u8arr], filename, { type: mime });
  };

  const handleUpCoordinates = (data) => {
    let formData = new FormData();

    formData.append("file", fileOrigin);
    let coordinates_text = "[";
    data.forEach((e) => {
      coordinates_text += "[" + e + "],";
    });
    formData.append(
      "coordinates_text",
      coordinates_text.substring(0, coordinates_text.length - 1) + "]"
    );

    ExternalApi.post(
      "http://namecard-ai.softone.asia/api/name-card-detection/",
      formData
    ).then(async (res) => {
      const fileTmp = dataURLtoFile(
        res.data.background,
        Math.random().toString(36).substr(2, 6)
      );
      await setFileResult(await getBase64(fileTmp));
      setCurrentStep(2);
      setPopupInfo(res.data.infor_predict);
      setDataPreview(res.data);
    });
  };

  const updateDatePreview = (index, data) => {
    let newDataPreview = dataPreview;
    newDataPreview.infor_predict[index] = { ...data };
    setDataPreview(newDataPreview);
  };

  const renderStep = () => {
    switch (currentStep) {
      case 0:
        return (
          <Dragger {...props} className="upload-card">
            <p className="ant-upload-drag-icon">
              <InboxOutlined />
            </p>
            <p className="ant-upload-text">
              Click or drag file to this area to upload
            </p>
            <p className="ant-upload-hint">
              Support for a single or bulk upload. Strictly prohibit from
              uploading company data or other band files
            </p>
          </Dragger>
        );
      case 1:
        return (
          <DropArea
            previewImg={previewImg}
            inforPredict={inforPredict}
            list={list}
            setList={setList}
            showTest={showTest}
            handleUpCoordinates={handleUpCoordinates}
            radioScale={radioScale}
          />
        );
      case 2:
        return (
          <Result
            previewImg={previewImg}
            list={list}
            popupInfo={popupInfo}
            fileResult={fileResult}
            showTest={showTest}
            dataPreview={dataPreview}
            updateDatePreview={updateDatePreview}
            radioScale={radioScale}
          ></Result>
        );
      default:
    }
  };

  return (
    <div className="home-page">
      <Steps current={currentStep} className="step-name-card">
        <Step title="Upload image" />
        <Step title="Fix crop box" />
        <Step title="Result" />
      </Steps>
      {renderStep()}
    </div>
  );
}

export default Home;
