import React from "react";
import ReactDOM from "react-dom";
import classNames from "classnames";
import Button from "../../components/button";
import Draggable from "./draggable";

class DropArea extends React.Component {
  constructor(props) {
    super(props);

    this.onDragOver = this.onDragOver.bind(this);
    this.onDrop = this.onDrop.bind(this);
    this.updateStateDragging = this.updateStateDragging.bind(this);
    this.updateStateResizing = this.updateStateResizing.bind(this);
    this.funcResizing = this.funcResizing.bind(this);
    this.handleAddBoundingBox = this.handleAddBoundingBox.bind(this);
    this.handleRemoveBoundingBox = this.handleRemoveBoundingBox.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);

    this.state = {
      list: [
        // { id: 1, isDragging: false, isResizing: false, top: 100, left: 50, width: 100, height: 150, name: 'test 1', color: 'red' }
        // { id: 2, isDragging: false, isResizing: false, top: 50, left: 200, width: 200, height: 100, name: 'test 2', color: 'green' }
      ],
      previewImg: "",
    };
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    const { previewImg, inforPredict, list, setList } = this.props;

    if (previewImg !== this.state.previewImg) {
      this.setState({
        previewImg,
        list: [],
      });
    }

    if (!list?.length && inforPredict?.length) {
      inforPredict.forEach((e, index) => {
        const item = {
          id: index + 1,
          isDragging: false,
          isResizing: false,
          top: e[1],
          left: e[0],
          width: e[2] - e[0],
          height: e[3] - e[1],
          name: "image test",
          color: "#ff4",
        };

        list.push(item);
      });

      setList(list);
      this.setState({ list });
    }
  }

  onDragOver(e) {
    e.preventDefault();
    return false;
  }

  onDrop(e) {
    let obj = JSON.parse(e.dataTransfer.getData("application/json"));

    let { list, setList } = this.props;

    let index = list.findIndex((item) => item.id === obj.id);
    list[index].isDragging = false;
    list[index].top = e.clientY - obj.y;
    list[index].left = e.clientX - obj.x;

    setList(list);
    this.setState({ list });

    e.preventDefault();
  }

  updateStateDragging(id, isDragging) {
    let { list, setList } = this.props;

    let index = list.findIndex((item) => item.id === id);
    list[index].isDragging = isDragging;

    setList(list);
    this.setState({ list });
  }

  updateStateResizing(id, isResizing) {
    let { list, setList } = this.props;

    let index = list.findIndex((item) => item.id === id);
    list[index].isResizing = isResizing;

    setList(list);
    this.setState({ list });
  }

  funcResizing(id, clientX, clientY) {
    let node = ReactDOM.findDOMNode(this.refs[`node_${id}`]);

    let { list, setList } = this.props;
    let index = list.findIndex((item) => item.id === id);
    list[index].width = clientX - node.offsetLeft - 120;
    list[index].height = clientY - node.offsetTop - 110;

    setList(list);
    this.setState({ list });
  }

  handleAddBoundingBox() {
    const { list, setList } = this.props;
    if (!list?.length) return;

    list.push({
      id: Math.random().toString(36).substr(2, 6),
      isDragging: false,
      isResizing: false,
      top: 0,
      left: 0,
      width: 100,
      height: 30,
      name: "",
    });

    setList(list);
    this.setState({ list });
  }

  handleRemoveBoundingBox(id) {
    const { list, setList } = this.props;
    if (!list?.length) return;

    const index = list.findIndex((e) => e.id === id);
    list.splice(index, 1);

    setList(list);
    this.setState({ list });
  }

  handleSubmit() {
    const { list, handleUpCoordinates } = this.props;
    if (!list?.length) return;

    const coordinates = list.map((e) => [
      e.left * this.props.radioScale,
      e.top * this.props.radioScale,
      (e.left + e.width) * this.props.radioScale,
      (e.top + e.height) * this.props.radioScale,
    ]);
    handleUpCoordinates(coordinates);
  }

  render() {
    const { previewImg } = this.props;
    const { list } = this.state;

    var i = new Image();
    i.src = previewImg;
    i.onload = function () {
      if (this.state?.iWidth && this.state?.iHeight && i.width && i.height) {
        this.setState({ iWidth: i.width, iHeight: i.height });
      }
    };

    let items = [];
    for (let item of list) {
      console.log(item);
      console.log(this.props.radioScale);
      items.push(
        <Draggable
          ref={`node_${item.id}`}
          key={item.id}
          id={item.id}
          top={item.top}
          left={item.left}
          width={item.width}
          height={item.height}
          iWidth={this.state?.iWidth || ""}
          isDragging={item.isDragging}
          isResizing={item.isResizing}
          name={item.name}
          color={item.color}
          showTest={this.props.showTest}
          updateStateDragging={this.updateStateDragging}
          updateStateResizing={this.updateStateResizing}
          funcResizing={this.funcResizing}
          radioScale={this.props.radioScale}
          handleRemoveBoundingBox={this.handleRemoveBoundingBox}
        />
      );
    }

    return (
      <div className="box-drop-area">
        <div className={classNames("preview-img", { active: previewImg })}>
          <img
            src={previewImg}
            alt=""
            className="img-bounding"
            style={{ width: "1000px" }}
          />
          <div
            className="drop-area"
            onDragOver={this.onDragOver}
            onDrop={this.onDrop}
          >
            {items}
          </div>
        </div>

        <div className="box-action">
          <Button
            size="large"
            type="primary"
            onClick={this.handleAddBoundingBox}
          >
            Add bounding box
          </Button>
          <Button size="large" type="primary" onClick={this.handleSubmit}>
            Submit
          </Button>
        </div>
      </div>
    );
  }
}

export default DropArea;
