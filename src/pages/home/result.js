import React from "react";
import Rectangle from "./rectangle";
import classNames from "classnames";
import { ExternalApi } from "../../api/endpoint";
import { Modal } from "antd";
import { getBase64 } from "../../utils/file-reader";
let count = 0;
const dataURLtoFile = (dataUrl, filename) => {
  const formatData = `data:text/csv;base64,${dataUrl}`;
  let arr = formatData.split(",");
  let mime = arr[0].match(/:(.*?);/)[1];
  let bstr = atob(arr[1]);
  let n = bstr.length;
  let u8arr = new Uint8Array(n);

  while (n--) {
    u8arr[n] = bstr.charCodeAt(n);
  }

  return new File([u8arr], filename, { type: mime });
};
class Result extends React.Component {
  constructor(props) {
    super(props);
    this.onDragOver = this.onDragOver.bind(this);
    this.state = { modal: false, imagePreviewLink: "" };
  }

  onDragOver(e) {
    e.preventDefault();
    return false;
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevProps.dataPreview !== this.props.dataPreview) {
      this.setState({ dataPreview: this.props.dataPreview });
    }
  }

  handleClickPreview = () => {
    let dataPreview = { ...this.props.dataPreview };
    console.log(dataPreview, "data previewwwwwwwwwww");
    if (dataPreview.infor_predict) {
      dataPreview["ratio_scale"] = this.props.radioScale;
      dataPreview.infor_predict.filter((item) => {
        delete item.imageLink;
        delete item.prob;
        delete item.tag;
        return { ...item };
      });
    }
    if (count === 0) {
      dataPreview.infor_predict = dataPreview.infor_predict.map(
        (infoPredict) => {
          count++;
          infoPredict.coordinate_text = infoPredict.coordinate_text.map(
            (item) => item / this.props.radioScale
          );
          infoPredict.coordinates = infoPredict.coordinates.map(
            (item) => item / this.props.radioScale
          );
          return infoPredict;
        }
      );
    }

    ExternalApi.post(
      "http://namecard-ai.softone.asia/api/name-card-detection/draw-from-infor",
      JSON.stringify(dataPreview)
    ).then(async (res) => {
      const fileTmp = dataURLtoFile(
        res.data,
        Math.random().toString(36).substr(2, 6)
      );
      let linkImg = await getBase64(fileTmp);
      this.setState({
        modal: true,
        imagePreviewLink: linkImg,
      });
    });
  };

  onClose = () => {
    this.setState({ modal: false });
  };

  onOk = () => {
    let img = new Image();

    img.crossOrigin = "anonymous"; // This tells the browser to request cross-origin access when trying to download the image data.
    // ref: https://developer.mozilla.org/en-US/docs/Web/HTML/CORS_enabled_image#Implementing_the_save_feature
    img.src = this.state?.imagePreviewLink;
    img.onload = () => {
      // create Canvas
      const canvas = document.createElement("canvas");
      const ctx = canvas.getContext("2d");
      canvas.width = img.width;
      canvas.height = img.height;
      ctx.drawImage(img, 0, 0);
      // create a tag
      const a = document.createElement("a");
      a.download = "download.png";
      a.href = canvas.toDataURL("image/png");
      a.click();
    };
  };

  render() {
    let items = [];
    let i = 0;
    for (let item of this.props.list) {
      items.push(
        <Rectangle
          ref={`node_${item.id}`}
          key={item.id}
          id={item.id}
          top={item.top}
          left={item.left}
          width={item.width}
          height={item.height}
          isDragging={item.isDragging}
          isResizing={item.isResizing}
          name={item.name}
          color={item.color}
          showTest={this.props.showTest}
          popupInfo={this.props.popupInfo[i]}
          index={i}
          updateDatePreview={this.props.updateDatePreview}
        />
      );
      i++;
    }
    return (
      <div className="box-drop-area">
        <button onClick={this.handleClickPreview}>Preview</button>
        <p>Detection Result</p>
        <div
          className={classNames("preview-img", {
            active: this.props.previewImg,
          })}
        >
          <img
            src={this.props.previewImg}
            alt=""
            className="img-bounding"
            style={{ width: "1000px" }}
          />
          <div className="drop-area" onDragOver={this.onDragOver}>
            {items}
          </div>
        </div>
        {/* <img src={this.props.previewImg}></img> */}
        <p>Background image</p>
        <img src={this.props.fileResult} style={{ width: "1000px" }}></img>
        <Modal
          title="Basic Modal"
          visible={this.state.modal}
          onOk={this.onOk}
          onCancel={this.onClose}
          okText="Save"
          width={850}
          height={700}
        >
          <img
            style={{ maxWidth: "791px", display: "center" }}
            src={
              this.state?.imagePreviewLink ? this.state?.imagePreviewLink : ""
            }
          ></img>
        </Modal>
      </div>
    );
  }
}

export default Result;
