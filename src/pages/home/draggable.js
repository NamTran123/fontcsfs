import React from "react";
import { Input } from "antd";
import { CloseCircleOutlined } from "@ant-design/icons";
import Resize from "./resize";

class Draggable extends React.Component {
  constructor(props) {
    super(props);
  }

  onMouseDown(e) {
    let elm = document.elementFromPoint(e.clientX, e.clientY);
    if (elm.className !== "resizer") {
      this.props.updateStateDragging(this.props.id, true);
    }
  }

  onMouseUp() {
    this.props.updateStateDragging(this.props.id, false);
  }

  onDragStart(e) {
    const nodeStyle = this.refs.node.style;
    e.dataTransfer.setData(
      "application/json",
      JSON.stringify({
        id: this.props.id,
        // mouse position in a draggable element
        x: e.clientX - parseInt(nodeStyle.left),
        y: e.clientY - parseInt(nodeStyle.top),
      })
    );
  }

  onDragEnd() {
    this.props.updateStateDragging(this.props.id, false);
  }

  render() {
    const {
      id,
      isDragging,
      isResizing,
      top,
      left,
      width,
      height,
      name,
      color,
      updateStateResizing,
      funcResizing,
      handleRemoveBoundingBox,
    } = this.props;
    console.log(this.props.iWidth, "iwidth...");
    const styles = {
      top,
      left,
      width,
      height,
      color,
    };

    return (
      <div
        ref="node"
        draggable={isDragging}
        id={`item_${id}`}
        className="item unselectable box-item"
        style={styles}
        onMouseDown={this.onMouseDown.bind(this)}
        onMouseUp={this.onMouseUp.bind(this)}
        onDragStart={this.onDragStart.bind(this)}
        onDragEnd={this.onDragEnd.bind(this)}
      >
        {this.props.showTest && <Input className="name-item" value={name} />}
        <Resize
          ref="resizerNode"
          id={id}
          isResizing={isResizing}
          resizerWidth={16}
          resizerHeight={16}
          updateStateResizing={updateStateResizing}
          funcResizing={funcResizing}
          radioScale={this.props.radioScale}
        />
        <CloseCircleOutlined
          className="close-box"
          onClick={() => handleRemoveBoundingBox(id)}
        />
      </div>
    );
  }
}

export default Draggable;
