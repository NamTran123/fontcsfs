import React from "react";
import { Input } from "antd";
import { CloseCircleOutlined } from "@ant-design/icons";
import Resize from "./resize";
import { Modal } from "antd";
import { Fragment } from "react/cjs/react.production.min";
import "./rectangle.css";
import { getBase64 } from "../../utils/file-reader";
import { ExternalApi } from "../../api/endpoint";

const dataURLtoFile = (dataUrl, filename) => {
  const formatData = `data:text/csv;base64,${dataUrl}`;
  let arr = formatData.split(",");
  let mime = arr[0].match(/:(.*?);/)[1];
  let bstr = atob(arr[1]);
  let n = bstr.length;
  let u8arr = new Uint8Array(n);

  while (n--) {
    u8arr[n] = bstr.charCodeAt(n);
  }

  return new File([u8arr], filename, { type: mime });
};
class Rectangle extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false,
      popupInfo: { ...this.props?.popupInfo, textType: "text" } || {
        textType: "text",
      },
      fontChecked: 0,
    };
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevProps.popupInfo !== this.props.popupInfo) {
      this.setState({
        popupInfo: { ...this.props.popupInfo, textType: "text" },
      });
    }
    if (!this.state?.popupInfo["imageLink"]) {
      this.fetchImage();
    }
  }

  componentDidMount() {
    this.setState({
      modal: false,
      popupInfo: { ...this.props.popupInfo, textType: "text" },
    });
  }

  fetchImage = () => {
    if (this.props?.popupInfo?.font && this.props?.popupInfo?.text) {
      ExternalApi.post(
        "http://namecard-ai.softone.asia/api/name-card-detection/draw-from-fonts",
        JSON.stringify({
          size: 70,
          fonts: this.props?.popupInfo?.font,
          content: this.props?.popupInfo?.text,
        })
      ).then(async (res) => {
        let newPopupInfo = this.props.popupInfo;
        let newData = [...res.data];
        newPopupInfo["imageLink"] = [];
        for (let i = 0; i < res.data.length; i++) {
          const fileTmp = dataURLtoFile(
            newData[i],
            Math.random().toString(36).substr(2, 6)
          );
          let base64Link = await getBase64(fileTmp);
          newPopupInfo["imageLink"].push(base64Link);
        }
        this.setState({ popupInfo: { ...newPopupInfo, textType: "text" } });
      });
    }
  };

  showpopup = () => {
    this.setState({
      modal: true,
      popupInfo: {
        ...this.props.popupInfo,
        textType: this.state.popupInfo.textType,
      },
    });
  };

  closepopup = () => {
    this.setState({ modal: false });
  };

  sizeUpate = (e) => {
    let newpopupInfo = { ...this.state.popupInfo };
    newpopupInfo["size"] = Number(e.target.value);
    this.setState({ popupInfo: newpopupInfo });
  };

  textUpate = (e) => {
    let newpopupInfo = { ...this.state.popupInfo };
    newpopupInfo["text"] = e.target.value;
    this.setState({ popupInfo: newpopupInfo });
  };

  colorUpate = (e) => {
    let newpopupInfo = { ...this.state.popupInfo };
    newpopupInfo["color"][0] = e.target.value;
    console.log(e.target.value);
    this.setState({ popupInfo: newpopupInfo });
  };

  textType = (e) => {
    console.log(this.state.popupInfo, "popupinfo state");
    console.log(e.target.value, "e.target.valu");
    let newpopupInfo = { ...this.state.popupInfo };
    newpopupInfo["textType"] = e.target.value;
    this.setState({ popupInfo: newpopupInfo });
  };

  fontSelect = (e) => {
    console.log(e.target.value, "font click");
    console.log(
      this.props.popupInfo.font.indexOf(e.target.value),
      "index of font click"
    );
    this.setState({
      fontChecked: this.props.popupInfo.font.indexOf(e.target.value),
    });
  };

  onOk = () => {
    let popupDataOk = { ...this.state.popupInfo };
    console.log(popupDataOk, "popupDataOk--truoc khi convert font");
    console.log(this.state.fontChecked, "font fontChecked");
    let tempFont = popupDataOk.font[0];
    popupDataOk.font[0] = popupDataOk.font[this.state.fontChecked];
    popupDataOk.font[this.state.fontChecked] = tempFont;
    this.props.updateDatePreview(this.props.index, { ...popupDataOk });
    this.setState({ modal: false, imageLink: undefined, fontChecked: 0 });
  };

  render() {
    console.log(
      typeof this.state?.popupInfo?.textType,
      "typeof this.state.popupinfo.texttype"
    );
    const {
      id,
      isDragging,
      isResizing,
      top,
      left,
      width,
      height,
      name,
      color,
      updateStateResizing,
      funcResizing,
    } = this.props;
    const styles = {
      top,
      left,
      width,
      height,
      color,
    };
    let colorInput =
      this.props?.popupInfo?.color &&
      this.props?.popupInfo?.color.length !== undefined
        ? this.props.popupInfo.color[0]
        : "";
    let font = this.props?.popupInfo?.font
      ? this.props.popupInfo.font.map((font, index) => {
          return (
            <div className="row-item" key={index + font + ""}>
              <label htmlFor="font4" className="radius-label">
                Font: {font}
              </label>
              <img
                src={
                  this.props?.popupInfo?.imageLink
                    ? this.props?.popupInfo?.imageLink[index]
                    : ""
                }
                className="font-img"
              />
              <input
                checked={this.state?.fontChecked === index ? "checked" : ""}
                type="radio"
                id={index + font + ""}
                name={"font" + this.props?.popupInfo?.text}
                value={font}
                onChange={this.fontSelect}
              />
            </div>
          );
        })
      : [];
    return (
      <Fragment>
        <div
          ref="node"
          draggable={isDragging}
          id={`item_${id}`}
          className="item unselectable box-item"
          style={styles}
          onClick={this.showpopup}
        >
          {this.state.showTest && <Input className="name-item" value={name} />}
        </div>
        <Modal
          title="Basic Modal"
          visible={this.state.modal}
          onOk={this.onOk}
          onCancel={this.closepopup}
          width={1000}
          height={700}
        >
          {" "}
          <p style={{ margin: "10px" }}>ID-5</p>
          <div className="row-item">
            <label htmlFor="content" className="radius-label">
              Content:
            </label>
            <input
              type="text"
              id="content"
              name="content"
              value={this.state.popupInfo?.text || ""}
              onChange={(e) => this.textUpate(e)}
            />
          </div>
          <div className="row-item">
            <label htmlFor="textSize" className="radius-label">
              Text size:
            </label>
            <input
              type="text"
              id="textSize"
              name="textSize"
              value={this.state.popupInfo?.size || ""}
              onChange={this.sizeUpate}
            />
          </div>
          <div className="row-item">
            <label htmlFor="textColor" className="radius-label">
              Text color
            </label>
            <input
              type="text"
              id="textColor"
              name="textColor"
              value={colorInput}
              onChange={this.colorUpate}
            />
          </div>
          <div className="row-item">
            <p className="radius-label">Text Type</p>
            <input
              type="radio"
              id="textType"
              name={this.state.popupInfo?.text}
              value="text"
              onChange={this.textType}
              checked={
                this.state.popupInfo.textType === "text" ? "checked" : ""
              }
            />
            <label for="textType">Text</label>
            <input
              type="radio"
              id="iconType"
              name={this.state.popupInfo?.text}
              value="icon"
              onChange={this.textType}
              checked={
                this.state.popupInfo.textType === "icon" ? "checked" : ""
              }
            />
            <label for="iconType">Icon</label>
          </div>
          {font}
        </Modal>
      </Fragment>
    );
  }
}

export default Rectangle;
