import React from "react";

class Resize extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    window.addEventListener("mousemove", this.onMouseMove.bind(this), false);
    window.addEventListener("mouseup", this.onMouseUp.bind(this), false);
  }

  componentWillUnmount() {
    window.removeEventListener("mousemove", this.onMouseMove.bind(this), false);
    window.removeEventListener("mouseup", this.onMouseUp.bind(this), false);
  }

  onMouseDown() {
    this.props.updateStateResizing(this.props.id, true);
  }

  onMouseMove(e) {
    if (this.props.isResizing) {
      this.props.funcResizing(this.props.id, e.clientX, e.clientY);
    }
  }

  onMouseUp(e) {
    if (this.props.isResizing) {
      this.props.updateStateResizing(this.props.id, false);
    }
  }

  render() {
    const style = {
      width: this.props.resizerWidth,
      height: this.props.resizerHeight,
    };

    return (
      <div
        className="resizer"
        style={style}
        onMouseDown={this.onMouseDown.bind(this)}
      />
    );
  }
}

export default Resize;
